#ifndef WRAPPER_MPI_H
#define WRAPPER_MPI_H

#include "futile_cst.h"
#include <stdbool.h>
#include "f_refcnts.h"
#include "dict.h"

F_DEFINE_TYPE(mpi_environment);

f90_mpi_environment_pointer f90_mpi_environment_copy_constructor(const f90_mpi_environment* other);

f90_mpi_environment_pointer f90_mpi_environment_type_new(const f90_f_reference_counter* refcnt,
  const int (*igroup),
  const int (*iproc),
  const int (*mpi_comm),
  const int (*ngroup),
  const int (*nproc));

void f90_mpi_environment_free(f90_mpi_environment_pointer self);

f90_mpi_environment_pointer mpi_environment_null(void);

f90_mpi_environment_pointer mpi_environment_comm(const int (*comm));

void release_mpi_environment(f90_mpi_environment* mpi_env);

void mpi_environment_set(f90_mpi_environment* mpi_env,
  int iproc,
  int nproc,
  int mpi_comm,
  int groupsize);

void mpi_environment_dict(const f90_mpi_environment* mpi_env,
  f90_dictionary_pointer dict_info);

void mpi_environment_set1(f90_mpi_environment* mpi_env,
  int iproc,
  int mpi_comm,
  int groupsize,
  int ngroup);

#endif
