#ifndef F_REFCNTS_H
#define F_REFCNTS_H

#include "futile_cst.h"
#include <stdbool.h>
#include "dict.h"

F_DEFINE_TYPE(f_reference_counter);

f90_f_reference_counter_pointer f90_f_reference_counter_copy_constructor(const f90_f_reference_counter* other);

/* void f90_f_reference_counter_type_new(f90_dictionary_pointer (*info)); */

bool f_associated(const f90_f_reference_counter* ref);

f90_f_reference_counter_pointer f_ref_null(void);

f90_f_reference_counter_pointer f_ref_new(const char* id,
  const void* (*address));

int f_ref_count(const f90_f_reference_counter* ref);

void refcnts_errors(void);

void nullify_f_ref(f90_f_reference_counter* ref);

void f_unref(f90_f_reference_counter* ref,
  int (*count));

void f_ref_free(f90_f_reference_counter* ref);

void f_ref(f90_f_reference_counter* src);

void f_ref_associate(const f90_f_reference_counter* src,
  f90_f_reference_counter* dest);

#endif
