subroutine f_free_ptr_i1(ptr)
  use dynamic_memory
  integer, dimension(:), pointer :: ptr

  call f_free_ptr(ptr)
end subroutine f_free_ptr_i1

subroutine f_free_ptr_d2(ptr)
  use dynamic_memory
  use f_precisions, only: f_double
  real(kind = f_double), dimension(:,:), pointer :: ptr

  call f_free_ptr(ptr)
end subroutine f_free_ptr_d2
