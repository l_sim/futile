program test_hooks
  use my_object
  use module_f_objects
  use yaml_output
  type(my_object_type) :: o, o2
  type(kernel_ctx) :: kernel
  external :: objtoyaml, objadd, changed1, changed2

  integer :: sid
  interface
     subroutine setLabel(obj, label)
       use my_object
       type(my_object_type), intent(in) :: obj
       character(len = 64), intent(out) :: label
     end subroutine setLabel
  end interface

  call f_lib_initialize()

  call my_object_type_init()

  call f_object_kernel_new(kernel, objtoyaml, 1)
  call f_object_signal_connect("my_object", "serialize", kernel, sid)

  call f_object_kernel_new(kernel, objadd, 2)
  call f_object_signal_connect("my_object", "add", kernel, sid)

  call f_object_kernel_new(kernel, changed1, 1)
  call f_object_signal_connect("my_object", "value-changed", kernel, sid)

  call f_object_kernel_new(kernel, changed2, 2)
  call f_object_kernel_add_str(kernel, "new value")
  call f_object_signal_connect("my_object", "value-changed", kernel, sid)

  call f_object_kernel_new(kernel, setLabel, 2)
  call f_object_signal_connect("my_object", "query-label", kernel, sid)

  o%a_value = 42
  call serialize(o)

  o2%a_value = 41
  call add(o, o2)
  call serialize(o)

  call query(o)
  call yaml_map("obj label", o%label)

  call f_lib_finalize()
end program test_hooks
