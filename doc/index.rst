.. futile documentation master file, created by
   sphinx-quickstart on Tue Aug 21 16:29:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _futile_index:

Welcome to futile's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Modules:

   f_prec 

   yaml_str

   inputfile

   memory

   yaml

   pyfutile

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Continuous integration
----------------------

.. image:: https://gitlab.com/l_sim/futile/badges/devel/pipeline.svg
   :target: https://gitlab.com/l_sim/futile/-/commits/devel

.. image:: https://gitlab.com/l_sim/futile/badges/devel/coverage.svg
   :target: https://l_sim.gitlab.io/futile/report
